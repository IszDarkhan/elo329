#ifndef SIMPLEBUTTON_H
#define SIMPLEBUTTON_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class SimpleButton; }
QT_END_NAMESPACE

class SimpleButton : public QWidget
{
    Q_OBJECT

public:
    SimpleButton(QWidget *parent = nullptr);
    ~SimpleButton();
public slots:
    void buttonClicked();

private:
    Ui::SimpleButton *ui;
};
#endif // SIMPLEBUTTON_H
