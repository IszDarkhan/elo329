#include "simplebutton.h"
#include "ui_simplebutton.h"
#include <QDebug>

SimpleButton::SimpleButton(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::SimpleButton)
{
    ui->setupUi(this);
}
void SimpleButton::buttonClicked(){
    qDebug() << "Botón presionado";
}
SimpleButton::~SimpleButton()
{
    delete ui;
}

